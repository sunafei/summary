* [首页](README.md)
* [java基础](java/java.md)
* [UML](uml/类图.md)
* [spring](spring/spring.md)
* [spring boot](java.md)
* [spring cloud](java.md)
* 数据库
** [oracle](db/oracle.md)
* linux
** [centeros常用命令](linux/centeros常用命令.md)
** [ubuntu常用命令](linux/ubuntu常用命令.md)
** [部署javaweb环境](linux/部署javaweb环境)
* [docker](docker/介绍.md)
** [安装](docker/安装.md)
** [常用命令](docker/常用命令.md)
* nginx
** [安装](nginx/安装.md)
** [配置文件](nginx/配置文件.md)
** [负载均衡](nginx/负载均衡.md)
** [高可用](nginx/高可用.md)
* [在线简历](cv/cv.md)
